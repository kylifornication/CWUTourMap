var directionsDisplay1;
var directionsService1 = new google.maps.DirectionsService();
var map;

function initialize1() {
  directionsDisplay1 = new google.maps.DirectionsRenderer();
  var ellensburg = new google.maps.LatLng(47.002824, -120.537860);
  var mapOptions = {
    zoom: 10,
    center: ellensburg
  }
  map = new google.maps.Map(document.getElementById('map-set'), mapOptions);
  directionsDisplay.setMap(map);
}


function calcRoute1() {
  var start1 = document.getElementById('start1').value;
  var end1 = document.getElementById('end1').value;
  var waypts = [];
  var checkboxArray = document.getElementById('waypoints');
  for (var i = 0; i < checkboxArray.length; i++) {
    if (checkboxArray.options[i].selected == true) {
      waypts.push({
          location:checkboxArray[i].value,
          stopover:true});
    }
  }

  var request = {
      origin: start1,
      destination: end1,
      waypoints: waypts,
      optimizeWaypoints: true,
      travelMode: google.maps.TravelMode.WALKING
  };
  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
      var route = response.routes[0];
      var summaryPanel = document.getElementById('directions_panel');
      summaryPanel.innerHTML = '';
      // For each route, display summary information.
      for (var i = 0; i < route.legs.length; i++) {
        var routeSegment = i + 1;
        summaryPanel.innerHTML += '<b>Trip: ' + routeSegment + '</b><br>';
        summaryPanel.innerHTML += route.legs[i].start_address + '<center><b> to </b></center>';
        summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
        summaryPanel.innerHTML += '<b>Length: </b>' + route.legs[i].distance.text + '<br><br>';
      }
	    TotalDistance(response);
      } else {
        alert("directions response "+status);
    }
  });
}
      function TotalDistance(result) {
      var totalTime = 0;
      var myroute = result.routes[0];
      for (i = 0; i < myroute.legs.length; i++) {
        totalTime += myroute.legs[i].duration.value;      
      }
      
      document.getElementById("total").innerHTML = "<b>Total Time: </b>" + (totalTime / 60).toFixed(2) + " minutes";
      }
	  
	  
google.maps.event.addDomListener(window, 'load', initialize1);
