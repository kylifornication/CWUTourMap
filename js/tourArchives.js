// JavaScript Document
$(document).on("pageinit",function(){
	
	$.ajax({
		url:"Buildings.xml",
		cache:false,
		dataType:"xml",
		success: function(xml){
			$('#buildingArchives').empty();
			
			$(xml).find('Buildings').each(function(){
				var info=  '<li data-id='+$(this).find("ID").text()
				+'><a href=#archives'
				+'><img src='+$(this).find("Photo").text()
				+'>'+$(this).find("Building").text()
				+'</h3><p>Type: '+$(this).find("Type").text()
				+'</p><p>Available: '+$(this).find("Available").text()
				+'</p></a></li>';
				$('#buildingArchives').append(info).listview('refresh');
			});
		}
	});
});
