// JavaScript Document
function tourform() {
	//First Name
	if(localStorage.getItem('fn')) {
		var usertext = 'First Name: '+localStorage.getItem('fn');
	}
	else {
		var usertext = 'No First Name entered.'
	}
	document.getElementById('firstName').innerHTML = usertext;
	//Last Name
	if(localStorage.getItem('ln')) {
		var usertext = 'Last Name: '+localStorage.getItem('ln');
	}
	else {
		var usertext = 'No Last Name entered.'
	}
	document.getElementById('lastName').innerHTML = usertext;
	//Parent Name
	if(localStorage.getItem('pn')) {
		var usertext = 'Parent Name: '+localStorage.getItem('pn');
	}
	else {
		var usertext = 'No Parent or Supervisor entered.'
	}
	document.getElementById('parentName').innerHTML = usertext;
	//Date
	if(localStorage.getItem('date')) {
		var usertext = 'Date: '+localStorage.getItem('date');
	}
	else {
		var usertext = 'No date entered.'
	}
	document.getElementById('date').innerHTML = usertext;
	
	//Year
	if(localStorage.getItem('YR')) {
		var usertext = localStorage.getItem('YR');
		var x = parseInt(usertext);
		usertext = 'No Year Selected';
		switch(x) {
			case 1:
			usertext = 'High School';
			break;
			case 2: 
			usertext = 'Freshman';
			break;
			case 3:
			usertext = 'Sophomore';
			break;
			case 4:
			usertext = 'Junior';
			break;
			case 5:
			usertext = 'Senior'
			break;
	}
	document.getElementById('year').innerHTML=usertext;
	}
}
	function clearlocalStorage() {
		localStorage.clear();
		window.location = 'index.html#tourSign';
	}
	function gohome() {
		localStorage.clear();
		window.location = 'index.html#search';
	}