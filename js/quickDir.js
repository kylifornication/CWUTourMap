// JavaScript Document
//Global Variables
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var map;
//Changes 11/6 @5:30
var start = []


//initializes the directions on map canvas, map center, and directions
function initialize() {
  directionsDisplay = new google.maps.DirectionsRenderer();
  var mapOptions = {
    zoom: 10,
    center: new google.maps.LatLng(47.002824, -120.537860)
  };

//Puts map in map-canvas and directions in theDirections
  var map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);
  directionsDisplay.setMap(map);
  directionsDisplay.setPanel(document.getElementById('theDirections'));
}


//Calculate the route based the start and stop values.
//Grab info from start and end option boxes
function calcRoute() {
  var start = document.getElementById('start').value;
  var end = document.getElementById('end').value;
//How to start the request in ONLY WALKING
  var request = {
    origin: start,
    destination: end,
    travelMode: google.maps.TravelMode.WALKING
  };
//Creates the route, displays message about the route (WARNING WALKING in beta)
  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
    }
  });
}

//DOMListener to make sure to run the function when the page is loaded.
google.maps.event.addDomListener(window, 'load', initialize);
