// JavaScript Document

$(document).ready(function(){
	$.ajax({
		url:"progressReport.xml",
		cache:false,
		dataType:"xml",
		success: function(xml){
			$('#progress').empty();
			
			$(xml).find('progressReport').each(function(){
				var info='<div><img src='
				+$(this).find("Photo").text()
				+' width="80" height="80"><br><h4>At a Glance: </h4>'
				+$(this).find("PlansataGlance").text()
				+'<br><h4>Completed: </h4> '
				+$(this).find("Completed").text()
				+'<br><h4>Tasks: </h4> '
				+$(this).find("Tasks").text()

				+'<br><hr>';
				
				
				$('#progress').append(info);
				
			});
		}
	});
});