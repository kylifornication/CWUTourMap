// JavaScript Document
$(document).on("pageinit",function(){
	
	$.ajax({
		url:"Buildings.xml",
		cache:false,
		dataType:"xml",
		success: function(xml){
			$('#buildingArchives').empty();
			
			$(xml).find('Buildings').each(function(){
				var info=  '<li data-id='+$(this).find("ID").text()
				+'><a href=#archives2'
				+'><img src='+$(this).find("Photo").text()
				+'>'+$(this).find("Building").text()
				+'</h3><p>Type: '+$(this).find("Type").text()
				+'</p><p>Available: '+$(this).find("Available").text()
				+'</p></a></li>';
				$('#buildingArchives').append(info).listview('refresh');
			});
		}
	});
	
$('#buildingArchives').on("click","li",function(){
	whichOne=($(this).attr("data-id"));
				
		$.ajax({
				url:"Buildings.xml",
				cache:false,
				dataType:"xml",
				success: function(xml1){
					
$(xml1).find('Buildings').each(function(){
	currentOne = ($(this).find("ID").text());
	currentBuilding = ($(this).find("Building").text());
	currentType = ($(this).find("Type").text());
	currentAvailable = ($(this).find("Available").text());
	currentPhoto = ($(this).find("Photo").text());
	currentHours = ($(this).find("Hours").text());
	
	if(currentOne==whichOne)
	{
		$('#theBuilding').text(currentBuilding);
		$('#theType').text(currentType);
		$('#theAvailable').text(currentAvailable);
		$('#thePhoto').html('<center><img src='+currentPhoto+' width="150" height="112" /></center>');
		$('#theHours').text(currentHours);
		
	}
});
				}
		});
})
});
	